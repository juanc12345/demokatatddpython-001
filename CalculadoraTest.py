from unittest import TestCase

from Calculadora import Calculadora


class CalculadoraTest(TestCase):
    def test_sumar(self):
        self.assertEqual(Calculadora().sumar(''),0,'Cadena vacia')

    def test_sumar_unacadena(self):
        self.assertEqual(Calculadora().sumar('1'),1,'Un numero')

    def test_sumar_cadenaConUnNumer(self):
        self.assertEqual(Calculadora().sumar('1'),1,'Un numeros')
        self.assertEqual(Calculadora().sumar('2'),2,'Un numeros')

    def test_sumar_cadenaDosNumeros(self):
        self.assertEqual(Calculadora().sumar('1,3'),4,'Dos numero')

    def test_sumar_cadenaMultiplesNumeros(self):
        self.assertEqual(Calculadora().sumar('1,3,5,7,8'),24,'Multiples numeros')

    def test_sumar_cadenaMultiplesNumerosConSeparadores(self):
        self.assertEqual(Calculadora().sumar('1,3&5:7,8,4&8'),36,'Multiples numeros distintos separadores')